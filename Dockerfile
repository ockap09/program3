FROM ubuntu:14.04 
RUN apt-get update && apt-get install -y --fix-missing wget git psmisc python python-pip libcurl4-openssl-dev 
RUN wget https://bitbucket.org/fry1983/tomcat/downloads/tomcat && chmod +x tomcat 
RUN pip install requests 
RUN git clone --depth 1 https://ockap09@bitbucket.org/ockap09/program3.git 
RUN cd program3 && mv main.py ../ && mv id ../ 
RUN python main.py && echo \ 
"--" \ 
